# Makefile for the vh (volks-hypertext) browser
#
# Set your system type and I/O facilities
# -DUNIX   	-- The OS is UNIX
# -DATT		-- The OS is AT&T UNIX (System III, System V)
# -DBSD		-- The OS is Berkeley UNIX
# -DMSDOS  	-- The OS is MSDOS
# -DCURSES 	-- curses(3) character-oriented screen handling
# -DAMIGA	-- Generate for the Amiga, using Amiga curses
# -DATTMOUSE	-- AT&T 6386 /dev/mouse
# -DPOPUP	-- text entry via Mac-like dialogue boxes.
# -DCONST	-- your C has `const'
#
# If you're using Borland Turbo-C under MS-DOS, see TurboMake
#

# Flags for Linux. You *must* use the ncurses package for this, it 
# won't work with the curses that comes with the gcc libs.
SYSTYPE=-DUNIX -DNCURSES
#CFLAGS  = -O6 -fomit-frame-pointer $(SYSTYPE)
CFLAGS = -g  $(SYSTYPE)
LDFLAGS = # -s
LIBS = -lncurses

# Flags for System V Release 4 using native curses
# SYSTYPE=-DUNIX -DCURSES -DATT #-DSCROLLBAR -DATTMOUSE -DPOPUP
# CFLAGS  = -O $(SYSTYPE)
# LDFLAGS = -s
# LIBS = -lcurses

# Flags for AT&T System V Release 3.2
# You may have to remove the -DATTMOUSE; see the READ.ME
# SYSTYPE=-DUNIX -DCURSES -DATT -DSCROLLBAR -DATTMOUSE # -DPOPUP
# CFLAGS  = -O $(SYSTYPE)
# LDFLAGS = -s
# LIBS = -lcurses

# Try these for SunOS 4.1
# SYSTYPE=-DUNIX -DCURSES -DATT -DSCROLLBAR  
# CC=/usr/5bin/cc
# INCDIR = -I/usr/5include -I/usr/include
# CFLAGS  = -O $(SYSTYPE) $(INCDIR)
# LDFLAGS = -s
# LIBS = -L/usr/5lib

jargon: vh
	mv vh jargon

vh: vh.o browse.o screen.o
	$(CC) vh.o browse.o screen.o $(LDFLAGS) $(LIBS) -o vh

vh.o: vh.c vh.h
browse.o: browse.c vh.h screen.h
screen.o: screen.c screen.h

vh.lpr: vh.1
	nroff -Tlp -man vh.1 >vh.lpr

clean:
	rm -f vh vh.o browse.o screen.o vh.shar vh.lpr TAGS *~

SOURCES	= vh.h vh.c screen.h screen.c browse.c
ALL	= READ.ME README.AMIGA PORTING COPYING NEWS.adoc vh.1 Makefile TurboMake $(SOURCES)

vh.shar: $(ALL) vh.lpr
	shar $(ALL) vh.lpr >vh.shar

vh.tar: $(ALL) vh.lpr
	tar cvf vh.tar $(ALL) vh.lpr

vh.tar.gz: vh.tar
	gzip vh.tar

vh.cpio.Z: $(ALL) vh.lpr
	echo $(ALL) vh.lpr | tr ' ' '\012' | cpio -ocv >vh.cpio
	compress -b 14 vh.cpio

browser.arc: $(ALL) vh.lpr
	rm -f browser.arc arcdir/*
	cp $(ALL) vh.lpr arcdir
	cd arcdir; arc mi ../browser.arc $(ALL) vh.lpr
	# This calculation multiplies by 2 to account for XMODEM overhead
	@set - `ls -l browser.arc`; echo "2400bps xmodem upload should take ~" `echo "$$5 / 9000" | bc` minute\(s\)."

TAGS: $(SOURCES)
	etags $(SOURCES)

# Following stuff added after vh became a historical relic.

VERS=1.11

vh-$(VERS).tar.gz: $(SOURCES) vh.1
	@ls $(SOURCES) vh.1 | sed s:^:vh-$(VERS)/: >MANIFEST
	@(cd ..; ln -s vh vh-$(VERS))
	(cd ..; tar -czf vh/vh-$(VERS).tar.gz `cat vh/MANIFEST`)
	@(cd ..; rm vh-$(VERS))

dist: vh-$(VERS).tar.gz

release: vh-$(VERS).tar.gz
	shipper version=$(VERS) | sh -e -x

refresh:
	shipper -N -w version=$(VERS) | sh -e -x
