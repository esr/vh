The code for the VH hypertext browser is currently known to work under
the following systems:

port				maintainer
------------------------------	--------------------------------------------
AT&T UNIX System V Release 3.2	Eric S. Raymond <esr@snark.thyrsus.com>
Dell UNIX System V Release 4	Eric S. Raymond <esr@snark.thyrsus.com>
Linux with ncurses 1.8.6	Eric S. Raymond <esr@snark.thyrsus.com>
DOS (with Borland C)		Ray Gardner
SunOS 4.1.1B			Joseph E. Sacco <sacco@mga.com>
BSD/386 1.0			Eric S. Raymond <esr@snark.thyrsus.com>

Expect the following files:

READ.ME		--- this one
NEWS            --- project history
vh.1		--- nroff source for the manual page
vh.lpr		--- preformatted version of above for non-UNIX systems
Makefile	--- UNIX makefile for the system
TurboMake	--- DOS makefile for the system (using Borland C)
vh.c		--- display-system-independent primitives
vh.h		--- interface file for vh.c
screen.c	--- screen and keyboard I/O support
screen.h	--- interface file for screen support
browse.c	--- command interpreter and curses code
README.AMIGA	--- porting notes on the Amiga version.

Copyright and license terms:
    Copyright (C) 1991 by Raymond D. Gardner and Eric S. Raymond
    Issued under BSD terms; see the file COPYING.

Porting notes:

The code consists of three modules.  The vh.c module contains only
display-independent code, and should be usable without change under
your OS (modulo possible differences in include files).  The browse.c
code includes the command interpreter; screen.c supplies curses(3)-like
screen handling with a few enhancements for mice.

The porting work basically consists of implementing screen.c.  I have
intentionally kept the curses usage simple; a complete description of
the facilities this program uses is included in the screen.c header.

I have done my best to make the porting process as cut and dried as possible.
This does not mean I consider the functional design of browse.c graven in
stone; if you think it needs improving, propose away.  But there
will be very definite advantages to keeping the screen- and
mouse-handling mechanics separate from the main command interpreter;
it should be possible using this organization to get a full-featured
and *uniform* interface on all the target machines.

The only part that's even moderately tricky is the mouse-handling
code, which is based around an event loop in egetch() that polls for
keystrokes or mouse events.

Here are some of the configuration #defines now present:

CRLFSIZE
   Either 1 or 2; if 1, vh.c assumes the OS uses \n as a line terminator,
if 2, it assumes \r\n.  The UNIX #define forces CRLFSIZE to 1,
otherwise 2 is assumed.

MSDOS
   Enables the MSDOS curses(3) lookalike code in screen.c.  If MSDOS
is *not* defined, some emulations of Borland library functions are
compiled in vh.c. Used in vh.c and screen.c.

UNIX
   Compiles some UNIX-dependent terminal-driver bashing into browse.c.

ATT
   Compiles code that tries to enable your interrupt character on
System V machines, in browse.c only.

BSD
   Tries to cope with the less capable BSD curses(3).
   As of release 1.6, we *no longer support* BSD curses(3).  This code is
still there, but if you have any problems with it, you're on your own.
The Right Thing is to acquire the ncurses package from netcom.com and
install that.  This will give you full System V Release 4 curses on your
Berkeley machine.

AMIGA
  This code came in at the 11th hour.  It supposedly enabled the
jargon browser to run on the Amiga, but hasn't been tested against vh.
See README.AMIGA for details.  Direct queries to Georg Sassen
<georg@bluemoon.GUN.de>.

SCROLLBAR
   if this is on, screen.c steals the last column to create an
on-screen scrollbar.  Note that this is implemented by decrementing
COLS; if you brew your own curses(3) replacement, you must make sure
this does not result in that last column's failing to get refreshed!

POPUP
   The POPUP #define disables the code that steals the bottom row of the
screen for use as a prompt and text-entry line.  If you define this,
the code will expect to be able to call a get_dialogue() function that
takes a prompt argument and returns a text string.  A get_dialogue()
implementation is provided for curses(3).
   Note: if POPUP is on, the -i option won't work.  Sorry about that!

MOUSE and ATTMOUSE
   Two other #defines, MOUSE and ATTMOUSE, control the compilation of
mouse-handling code.  The mouse on the AT&T 6386WGS is fully
supported.  If you have a /usr/include/sys/mouse.h that includes the
following lines

struct mouseinfo {       
	unsigned char	status;
	char	xmotion, ymotion;
};

/* Ioctl definitions */
#define MOUSEIOC        ('M'<<8)
#define MOUSEIOCREAD    (MOUSEIOC|60)

then the ATTMOUSE code will probably work for you unaltered.  If your
mouse driver interface looks anything like this, enablng the device
should be quite easy.  The MOUSE code provides most of the interfacing;
all you have to write is the poll-and-decode loop in egetch().

Porting to windows environments:

If you are porting to a windows environment, you may wish to turn off
SCROLLBAR and turn on POPUP and embed the text window in some kind of
text widget or toolkit object, and have the mouse click events
interpreted in the egetch() loop.  You'll alse have to write your own
get_dialogue() and message() functions; see browse.c for models.

Have fun!
			Eric Raymond = eric@snark.thyrsus.com (UNIX)
			Ray Gardner = FidoNet 1:104/89.2 (DOS)
			              rgardner@crs.org
