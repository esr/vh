/* screen.h -- interface to screen and keyboard handling */

#define MAXROWS	66	/* most lines per screen we'll try to deal with */
#define MAXCOLS	132	/* most characters per line we'll essay */

#ifdef ATTMOUSE
#define MOUSE		/* generic mouse handing */
#endif /* ATTMOUSE */

#if defined(BSD) && !defined(NCURSES)
#define OLDCURSES
#endif /* defined(BSD) && !defined(NCURSES) */

#if defined(CURSES) || defined(NCURSES)
#include <curses.h>
#else
typedef	char bool;
#define FALSE	0
#define TRUE	1
#define OK	0
#define ERR	-1

/*
 * This holds a character and attributes.  Below, we have to do
 * some grotty things with macros and casts in case sizeof(long)
 * != sizeof(int).
 */
typedef unsigned long chtype;	/* the idea is to force 32 bits */

extern int LINES, COLS;		/* terminal depth & width globals */
extern int COLORS, COLOR_PAIRS;	/* max color range globals */

extern int initscr();	/* init screen, set LINES and COLS */
extern int endwin();	/* de-initialize screen code */
extern int init_pair();	/* initialize color pair */
extern int clear();	/* clear screen */
extern int refresh();	/* refresh real screen from virtual screen */
extern int ungetch();	/* put char on queue to be grabbed by next getch() */
extern int inch();	/* get char from screen's current update location */
extern int getyx();	/* get current update cursor position */
extern int move();	/* move update cursor to row y colum x */
extern int _addch();	/* add char/attribute at update location */
#define addch(c)	_addch((chtype)(c))
extern int addstr();	/* write string from update location */
extern int attrset();	/* set attribute (A_NORMAL, A_REVERSE, A_BOLD) */
extern bool has_colors();	/* do we have color? */
extern int start_color();	/* enable color support */
extern int color_pair();	/* set up a color pair */	
extern int napms();	/* nap for some number of milliseconds */
extern void beep();	/* ring the terminal bell */

/* the (void)s are to suppress "Code has no effect" warnings */
#define	mvaddch(y,x,ch)		(void)(move(y,x) == ERR ? ERR : addch(ch))
#define	mvaddstr(y,x,str)	(void)(move(y,x) == ERR ? ERR : addstr(str))
#endif /* CURSES */

#ifndef A_COLOR
/* color defines */
#define A_COLOR		0xff0000
#define COLOR_PAIR(n)	((chtype)(n) << 16)
#define PAIR_NUMBER(n)	(((chtype)(n) & A_COLOR) >> 16)
#define COLOR_BLACK	0
#define COLOR_BLUE	1
#define COLOR_GREEN	2
#define COLOR_CYAN	3
#define COLOR_RED	4
#define COLOR_MAGENTA	5
#define COLOR_YELLOW	6
#define COLOR_WHITE	7
#endif /* A_COLOR */

#ifndef KEY_EXIT
/* special keystroke defines --- picked to coincide with curses(3) values  */
#define KEY_EXIT	0551	/* not on PC keyboard */
#define KEY_HELP	0553	/* not on PC keyboard */
#define KEY_REFRESH	0565	/* not on PC keyboard */
#define KEY_NPAGE	0522	/* PgDn */
#define KEY_PPAGE	0523	/* PgUp */
#define KEY_UP		0403	/* Up Arrow */
#define KEY_DOWN	0402	/* Down Arrow */
#define KEY_REFERENCE	0564	/* not on PC keyboard */
#define KEY_ENTER	0527	/* Enter */
#define KEY_RIGHT	0405	/* Right Arrow */
#define KEY_LEFT	0404	/* Left Arrow */
#define KEY_FIND	0552	/* not on PC keyboard */
#define KEY_SFIND	0605	/* not on PC keyboard */
#define KEY_SELECT	0601	/* not on PC keyboard */
#define KEY_HOME	0406	/* Home */
#define KEY_BEG		0542	/* not on PC keyboard */
#define KEY_END		0550	/* End */
#define KEY_UNDO	0630	/* not on PC keyboard */
#define KEY_PRINT	0532	/* not on PC keyboard */
#define KEY_FBASE	0410	/* function key base */
#define KEY_F(n)	(KEY_FBASE + (n))
#endif /* KEY_EXIT */

#ifndef ACS_HLINE
/* alternate char set --- indices picked to coincide with curses(3) values */
	/* VT100 symbols */
extern unsigned char acs_map[96];
#define ACS_BSSB	(acs_map[0])
#define ACS_SSBB	(acs_map[1])
#define ACS_BBSS	(acs_map[2])
#define ACS_SBBS	(acs_map[3])
#define ACS_SBSS	(acs_map[4])
#define ACS_SSSB	(acs_map[5])
#define ACS_SSBS	(acs_map[6])
#define ACS_BSSS	(acs_map[7])
#define ACS_BSBS	(acs_map[8])
#define ACS_SBSB	(acs_map[9])
#define ACS_SSSS	(acs_map[10])
#define ACS_ULCORNER	ACS_BSSB
#define ACS_LLCORNER	ACS_SSBB
#define ACS_URCORNER	ACS_BBSS
#define ACS_LRCORNER	ACS_SBBS
#define ACS_RTEE	ACS_SBSS
#define ACS_LTEE	ACS_SSSB
#define ACS_BTEE	ACS_SSBS
#define ACS_TTEE	ACS_BSSS
#define ACS_HLINE	ACS_BSBS
#define ACS_VLINE	ACS_SBSB
#define ACS_PLUS	ACS_SSSS
#define ACS_S1		(acs_map[11])	/* scan line 1 */
#define ACS_S9		(acs_map[12])	/* scan line 9 */
#define ACS_DIAMOND	(acs_map[13])	/* diamond */
#define ACS_CKBOARD	(acs_map[14])	/* checker board (stipple) */
#define ACS_DEGREE	(acs_map[15])	/* degree symbol */
#define ACS_PLMINUS	(acs_map[16])	/* plus/minus */
#define ACS_BULLET	(acs_map[17])	/* bullet */
	/* Teletype 5410v1 symbols */
#define ACS_LARROW	(acs_map[18])	/* arrow pointing left */
#define ACS_RARROW	(acs_map[19])	/* arrow pointing right */
#define ACS_DARROW	(acs_map[20])	/* arrow pointing down */
#define ACS_UARROW	(acs_map[21])	/* arrow pointing up */
#define ACS_BOARD	(acs_map[22])	/* board of squares */
#define ACS_LANTERN	(acs_map[23])	/* lantern symbol */
#define ACS_BLOCK	(acs_map[24])	/* solid square block */
#endif

#ifndef A_CHARTEXT
/* highlight defines */
#define A_CHARTEXT	0x00007f
#define A_ATTRIBUTES	0xffff00
#define A_NORMAL	0x000000
#define A_STANDOUT	0x000100
#define A_UNDERLINE	0x000200
#define A_REVERSE	0x000400
#define A_BLINK		0x000800
#define A_BOLD		0x001000
#define A_ALTCHARSET	0x002000
#endif /* A_CHARTEXT */

#ifdef OLDCURSES
typedef int chtype;
#define resetterm()	resetty()
#define saveterm()	savetty()
#define has_colors()	FALSE
#define GQDEPTH 	10
extern int egetch(), ungetch(), attrset();
#endif /* OLDCURSES */

/*
 * Mouse event handing.  The KEY_MOUSE define has to be distinct from
 * any keystroke we might want to process.
 *
 * SVr4 defines KEY_MOUSE; SVr3 does not.  If we don't get it from the
 * curses.h file, define it here.
 */
#ifndef KEY_MOUSE
#define KEY_MOUSE	0631

#define A_BUTTON_CHANGED	0x0f	/* any change in button status? */
#define BUTTON1_PRESSED		0x01	/* left mouse button down */
#define BUTTON1_RELEASED	0x02	/* left mouse button release */
#define BUTTON3_PRESSED		0x04	/* right mouse button down */
#define BUTTON3_RELEASED	0x08	/* right mouse button up */
#endif /* KEY_MOUSE */

/* we'll define these */
extern int egetch();		/* get mouse event or character, no echo */
extern void readscr();		/* get content of given region */
extern void mouse_init();	/* initialize mouse */
extern void mouse_move();	/* move mouse hot spot */
extern int mouse_status();	/* get mouse location and button state */
extern void mouse_show();	/* show mouse cursor */
extern void mouse_hide();	/* hide mouse cursor */
extern void mouse_color();	/* show mouse cursor in color? */

/* special coordinate values returned by mouse_status() */
#define CMDLINE		-1	/* command line of application window */
#define THUMBCOL	-1	/* thumb column of application window */

#ifdef AMIGA
#define     F_OK    0
typedef short chtype;
#define GQDEPTH 	10
#endif /* AMIGA */
  
/* handy names for special characters */
#define CTL(c)	((c) & 0x1f)
#define TAB	0x09	/* tab */
#define BS	0x08	/* backspace */
#define CR	0x0d	/* carriage return */
#define LF	0x0a	/* line feed */
#define ESC	0x1b	/* escape */
#define SP	0x20	/* space */
#define DEL	0x7f	/* delete */

/* screen.h ends here */
