.TH VH 1 "10 May 1993" "UNIX"
.IX "vh" "" "vh \(em -- volks-hypertext browser for the Jargon File"
.ds ud /usr/local/lib/vh
.SH NAME
vh \- volks-hypertext browser for the Jargon File
.SH SYNOPSIS
\fBjargon\fR [-cfgimrs] [-b key]
.br
\fBvh\fR [-cfgimrs] [-b key] [document]
.SH DESCRIPTION
This program is a handy hypertext browser, originally written for the
Jargon File but convenient for use with text files marked up in the
simple format documented below.  It allows you to page forward and back
through a document, and supports reference chasing so that you can
easily follow chains of "See" and "See also" pointers through the
text.

Normal usage is simply to type the name of your document to the shell.
Thus, `jargon' browses the Jargon File.  Interactive help explaining
the keyboard commands is available from the screen.  If the program
sees a mouse, a fairly complete `point-and-shoot' interface is also
available.  Color will be used if it is available, but may be
suppressed with the -m option.  If you are using this program under
DOS and your CGA develops snow when the video RAM is written directly,
use -s.

It is useful to know that there are two modes; one browses the text
file, the other the entry index (if you chase a selection in the entry
index you are popped to text mode at the corresponding entry).
.SH MOUSE USAGE
The following section applies only to systems with attached and active
mice.  If you are using a Mac or some other system with a deficient
mouse, the single mouse button is treated as a `left' button.

You will notice a scrollbar on the right edge of the screen.  One of
the characters of the scrollbar is different from the rest.  This is
called the `thumb' or `scroll box'.  As you move through the file, this
gives you an approximate indication of where you are in the file.  If
the thumb is one-third of the way down the screen, you are about
one-third of the way through the file.  It works in both the text and
index files.

If you have a mouse, you can click the left button directly on a
reference on the screen and jump to it.  You can also scroll or
page forward or backward by clicking on the scrollbar.  There are
up and down arrows above and below the scroll bar for scrolling
line by line.  Clicking on the scrollbar below the thumb will
page forward, and clicking above it will page back.  You can also
"drag" the thumb by clicking on it and holding, then moving up or
down before releasing.  This allows you to jump quickly through
the file.  Holding the mouse down on the arrows or above or below
the thumb will cause the scrolling or paging action to repeat
until you release the button.  This is one of only two places where
holding the button has any special effect (the other is described below).

Clicking the right button will cause the program to backtrack the
same way as the Esc or Backspace keys.

You may also click the left button on the function key prompts on
the bottom line to get the same effect as pressing the function
keys themselves.  The ^Q (Help) entry is special; if you click it
with the mouse, it will come up, but disappear when you let go of the
mouse button.  This is a feature, not a bug; if you want help to hang
out till you tell it to go away, type control-Q.

In the text file only, if you click on the main screen (i.e. not
on the scrollbar or the prompt line) outside of a reference in
curly braces, the program will page forward one screen.
.SH OTHER OPTIONS
The -i option enables an incremental-lookup feature (but note that
this feature may not be available in all ports).  In this mode, lookup
and string search both behave like an Emacs incremental search.  That
is, after you type each letter of a key, your viewpoint moves forward
to the next key (if any) containing the substring you've already typed.  If
there is no such key, the browser beeps and drops out of
incremental-lookup mode.  String search works similarly, behaving
almost exactly like an Emacs ^S.  As a reminder that you're in these
incremental modes, highlights do not light up on the screens you pass
through; when you want to leave the mode, press enter and the current
page's highlights will light up.

The -b option runs vh in batch mode to retrieve the entry
corresponding to a particular given key.  The entry, if any, is
written to standard output.

The -f (filter) option accepts a list of entry headwords on stdin and
sends those entries to stdout.

Finally, the -r option picks an entry at random and sends it to
standard output.  This may be useful for `fortune cookie' applications.
.SH INDEX GENERATION
Two data files, the default text and index for the Jargon File, should live
in \*(ud.  To generate an index from a new version, cd to this library
directory.  Copy or move the textfile to \*(ud/jargon.txt and run the
command
.B jargon -g.

For experimental purposes, it is possible to override the default
document name with a command-line argument.  Thus, assuming a file
.B example.tex
is in proper format, you may generate an index for
.I example.txt
with
.B vh -g example
and browse it with
.B vh example
.P
When you have the `example' database ready for public use, simply link
or copy vh to the name `example'.  If no arguments are given, this
program tries to browse a database with the name it was invoked under.

The -c mode does some format and consistency checks on a text/index
pair.  Use this if it looks as though your lookups are landing in the
wrong places; it will let you know if your index is out of date.
.SH TEXT FILE FORMAT
`Proper format' means:

1. References are surrounded by {} or {{}}.

If the first character of the text file is not a `=', the code also assumes:

2. Reference text may not begin with whitespace, single- or double-quote, nor
   end with a semicolon.

3. Reference targets begin with a `:' at the left margin and are bounded
on the right by another `:' (an optional second `:' may follow that).

These rules are useful for filtering out false hits from the index.
.SH FILES
A copy of vh called  `foo' would require the following:
.TP 3.5i
.I \*(ud/foo.txt
Text of your document.
.TP 3.5i
.I \*(ud/foo.idx
Index of your document.
.TP 3.5i
.I ./foo.cut
Where entries selected by the ^Y (Print) command are appended to.
.SH ENVIRONMENT VARIABLES
.TP 25
.I VHPATH
Search path for vh documents. Defaults to "\*(ud".
.SH BUGS
If a selection wraps around the right margin, only a mouse click on
the part before the line wrap will chase it; clicks on the portion
trailing into the next line simply cause a page forward.  This glitch
was judged more trouble to fix than it's worth.

Use of the ESC key inhibits refresh under some curses(3) versions;
thus, you might have to do a ^L to force screen update afterwards.
Crocking around this in the obvious way would introduce screen flicker for
everybody.  Use BS.

Because BSD UNIX's pseudorandom-number generator isn't very good, the
fortune-cookie mode under that UNIX isn't either.

Output from a -f list may include alphabetical section headers which
are not, strictly speaking, part of the desired text.
.SH AUTHORS
Concept, DOS support, and database code from the MS-DOS `jargon'
program by Raymond D. Gardner <FidoNet 1:104/89.2>; new features,
docs, and curses(3)-using UNIX code by Eric S. Raymond
<esr@snark.thyrsus.com>.  See http://www.tuxedo.org/~esr/
for updates and related resources.
.SH SEE ALSO
.I The New Hacker's Dictionary (Second Edition),
by Eric S. Raymond (ed.), MIT Press 1993, ISBN 0-262-68079-3.  The
Jargon File (version 3.0.0), nicely typeset and bound with additional
front matter.

The Jargon File can be obtained by FTP from various archive sites.
A copy lives more or less permanently as
.I pub/jargon?????.txt
at
.I prep.ai.mit.edu
(18.71.0.38), where `?????' is replaced by the current version number.
Future updates will be made available there.

Please email your Jargon File entries and corrections to
jargon@snark.thyrsus.com.
