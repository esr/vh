/* vh.h --- interface to display-independent retrieval code */

#define PATHLEN		256	/* maximum filename size */
#define	BTSMAX		100	/* backtrack stack size */
#define	NOPLACE		(-1)	/* out-of-bounds for screen coordinate */
#define NOWHERE		(-1L)	/* invalid file position */
#define	LNSZ		256	/* maximum text line width */
#define MAXWIDTH	256	/* maximum screen width */
#define	ITEMSMAX	2400	/* max index items per document */

#ifndef UNIX
typedef long daddr_t;
#else
#if !defined(AOS)
#include <unistd.h>
#endif /* !defined(AOS) */
#include <sys/types.h>
#endif /*  */

#ifndef SEEK_SET	/* AOS is bogus */
#define SEEK_SET	0
#define SEEK_END	2
#endif

/* index of last line on screen available for text display */
#ifdef POPUP
#define LASTLINE	(LINES - 1)
#else
#define LASTLINE	(LINES - 2)
#endif /* POPUP */

/* this structure appears at the beginning of each index */
typedef struct
{
    long	magic;		/* magic number, distinguishes file type */
    int		nfiles;		/* count of text files in the database */
				/* multi-files isn't implemented yet */
}
vhhdr;

/* this structure represents a select region */
typedef struct
{
    int	xl, yl, xr, yr;
}
region;

/*
 * FILEINFO -- everything we need to keep context of text and index
 */
typedef struct fileinfo
{
    FILE *fp;			/* file pointer */
    daddr_t endpos;		/* end file pos (== file length) */
    daddr_t toppos;		/* pos of new top line on screen */
    daddr_t dsptoppos;		/* pos of current top line on screen */
    daddr_t dspnextpos;		/* pos of line after last line on screen */
    daddr_t lastpagetoppos;	/* pos of top line of last page */
    daddr_t hitpos;		/* pos where string search hit */
    region sel;			/* selected region on screen (if any) */
    int ythumb;			/* current thumb row */
    int btscnt;			/* backtrack stack current count */
    daddr_t bts[BTSMAX];	/* backtrack stack of file positions */
    region selbts[BTSMAX];	/* backtrack stack of link selections */
}
FILEINFO;

/* entry points of vh.c */
extern void chkindex();		/* check for pathologies in file */
extern void mkindex();		/* generate index for file */
extern char *headword();	/* is given line a headword? */
extern bool initbrowse();	/* initialize browse */
extern void setlastpage();	/* set lastpagetoppos members */
extern daddr_t iflink();	/* test for presence of link */
extern daddr_t getnextln();	/* get next line */
extern daddr_t getprevln();	/* get previous line */
extern daddr_t xlocate();	/* go to term by name */
extern daddr_t ilocate();	/* incremental-locate term */
extern daddr_t ffind();		/* find string in file */
extern daddr_t ifind();		/* incremental-find string in file */
extern daddr_t jrandom();	/* return offset of random entry */
extern region findnextsel();	/* find next possible selection */
extern region findprevsel();	/* find previous possible selection */
extern void enqueue();		/* stack a location */
extern void dequeue();		/* unstack a location */
extern FILEINFO vhi;		/* file info for index */
extern FILEINFO *vht;		/* file info for text */

#if defined(__TURBOC__) && defined(FFGETS)
extern char *ffgets();		/* faster fgets */
#define fgets(s,n,f) ffgets((s), (n), (f))
#endif /* defined(__TURBOC__) && defined(FFGETS) */

/* vh.h ends here */
